package com.acaisoft.interviewapp.data.repositories

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface GithubRepositoriesApi {

    @GET("users/{username}/repos")
    fun fetchPublicRepositories(@Path("username") username: String): Call<List<GithubRepositoryResponse>>

    @GET("users/{username}")
    fun fetchUserData(@Path("username") username: String): Call<UserResponse>
}