package com.acaisoft.interviewapp.data.repositories

import com.google.gson.annotations.SerializedName

data class GithubRepositoryResponse(
    val id: Long,
    val name: String,
    val description: String,
    val forks: Int,
    val watchers: Int,
    @SerializedName("stargazers_count")
    val stars: Int,
    val language: String,
    val homepage: String,
    val fork: Boolean,
    val owner: UserResponse
)