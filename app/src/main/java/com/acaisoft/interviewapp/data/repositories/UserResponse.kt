package com.acaisoft.interviewapp.data.repositories

import com.google.gson.annotations.SerializedName

data class UserResponse(
    val id: Long,
    val name: String,
    val url: String,
    val email: String,
    val login: String,
    val location: String,
    @SerializedName("avatar_url")
    val avatarUrl: String
)