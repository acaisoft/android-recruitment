package com.acaisoft.interviewapp.presentation.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.acaisoft.interviewapp.di.ActivityComponent
import com.acaisoft.interviewapp.di.getDependencyInjector

abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val activityComponent = getDependencyInjector().newActivityComponent()
        setupDependencies(activityComponent)
    }

    protected abstract fun setupDependencies(activityComponent: ActivityComponent)
}