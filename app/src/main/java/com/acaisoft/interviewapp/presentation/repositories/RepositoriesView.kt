package com.acaisoft.interviewapp.presentation.repositories

import com.acaisoft.interviewapp.data.repositories.GithubRepositoryResponse

interface RepositoriesView {

    fun showRepositories(repositories: List<GithubRepositoryResponse>)
}