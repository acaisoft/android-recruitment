package com.acaisoft.interviewapp.presentation.repositories

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.acaisoft.interviewapp.R
import com.acaisoft.interviewapp.data.repositories.GithubRepositoryResponse

class RepositoriesAdapter : RecyclerView.Adapter<RepositoriesAdapter.ViewHolder>() {

    private val repositories: MutableList<GithubRepositoryResponse> = mutableListOf()

    fun replaceRepositories(repositories: List<GithubRepositoryResponse>) {
        this.repositories.clear()
        this.repositories.addAll(repositories)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.item_repository, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return repositories.size
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val repository = repositories[position]
        val context = viewHolder.contentLayout.context

        viewHolder.titleTextView.text = repository.name
        viewHolder.descriptionTextView.text = repository.description
        viewHolder.watchersTextView.text = context.getString(R.string.text_watchers, repository.watchers)
        viewHolder.starsTextView.text = context.getString(R.string.text_stars, repository.stars)
        viewHolder.forksTextView.text = context.getString(R.string.text_forks, repository.forks)
    }

    class ViewHolder(rootItemView: View) : RecyclerView.ViewHolder(rootItemView) {
        internal val contentLayout: View = rootItemView.findViewById(R.id.layout_content)
        internal val titleTextView: TextView = rootItemView.findViewById(R.id.text_repo_title)
        internal val descriptionTextView: TextView = rootItemView.findViewById(R.id.text_repo_description)
        internal val watchersTextView: TextView = rootItemView.findViewById(R.id.text_watchers)
        internal val starsTextView: TextView = rootItemView.findViewById(R.id.text_stars)
        internal val forksTextView: TextView = rootItemView.findViewById(R.id.text_forks)
    }
}