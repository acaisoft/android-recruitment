package com.acaisoft.interviewapp.presentation.repositories

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import com.acaisoft.interviewapp.R
import com.acaisoft.interviewapp.data.repositories.GithubRepositoryResponse
import com.acaisoft.interviewapp.di.ActivityComponent
import com.acaisoft.interviewapp.presentation.base.BaseActivity
import javax.inject.Inject

class RepositoriesActivity : BaseActivity(), RepositoriesView {

    @Inject
    internal lateinit var presenter: RepositoriesPresenter

    private lateinit var repositoriesRecyclerView: RecyclerView
    private lateinit var searchButton: ImageButton
    private lateinit var editTextUsername: EditText
    private lateinit var toolbar: Toolbar

    override fun setupDependencies(activityComponent: ActivityComponent) {
        activityComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repositories)

        //Set up ToolBar
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        repositoriesRecyclerView = findViewById(R.id.repos_recycler_view)
        // Set up search button
        searchButton = findViewById(R.id.button_search)
        searchButton.setOnClickListener(View.OnClickListener {
            // TODO - call RepositoriesPresenter::onSearchRepositoriesByUserClick()
        })
        // Set up username EditText
        editTextUsername = findViewById<EditText>(R.id.edit_text_username)
        editTextUsername.addTextChangedListener(mHideShowButtonTextWatcher)
        editTextUsername.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                // TODO - call RepositoriesPresenter::onSearchRepositoriesByUserClick()
                return@OnEditorActionListener true
            }
            false
        })

        setupRepositoriesRecyclerView(repositoriesRecyclerView)
    }

    private fun setupRepositoriesRecyclerView(repositoriesRecyclerView: RecyclerView) {
        repositoriesRecyclerView.layoutManager = LinearLayoutManager(this)
        repositoriesRecyclerView.adapter = RepositoriesAdapter()
    }

    override fun onStart() {
        super.onStart()
        presenter.bind(this)
    }

    override fun onStop() {
        presenter.unbind()
        super.onStop()
    }

    override fun showRepositories(repositories: List<GithubRepositoryResponse>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    private val mHideShowButtonTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(charSequence: CharSequence, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, start: Int, before: Int, count: Int) {
            searchButton.visibility = if (charSequence.isNotEmpty()) View.VISIBLE else View.GONE
        }

        override fun afterTextChanged(editable: Editable) {

        }
    }
}