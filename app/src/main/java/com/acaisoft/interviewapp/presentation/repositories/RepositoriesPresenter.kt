package com.acaisoft.interviewapp.presentation.repositories

import com.acaisoft.interviewapp.di.ActivityScope
import com.acaisoft.interviewapp.presentation.base.BasePresenter
import javax.inject.Inject

@ActivityScope
class RepositoriesPresenter @Inject constructor() : BasePresenter<RepositoriesView>() {

    fun onSearchRepositoriesByUserClick(userName: String) {
        // TODO - implement searching repositories by user and display them
    }
}