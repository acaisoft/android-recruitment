package com.acaisoft.interviewapp

import android.app.Application
import com.acaisoft.interviewapp.di.AppComponent
import com.acaisoft.interviewapp.di.AppComponentProvider
import com.acaisoft.interviewapp.di.DaggerAppComponent

class AcaisoftApp : Application(), AppComponentProvider {
    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .application(this)
            .build()
    }

    override fun provideAppComponent() = appComponent
}