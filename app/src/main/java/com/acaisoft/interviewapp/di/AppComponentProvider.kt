package com.acaisoft.interviewapp.di

import android.app.Activity

interface AppComponentProvider {

    fun provideAppComponent(): AppComponent
}

fun Activity.getDependencyInjector(): AppComponent {
    return (application as AppComponentProvider).provideAppComponent()
}