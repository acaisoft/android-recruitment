package com.acaisoft.interviewapp.di

import com.acaisoft.interviewapp.presentation.repositories.RepositoriesActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent
interface ActivityComponent {

    fun inject(activity: RepositoriesActivity)
}