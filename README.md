# Android Developer - recruiting task

The task is to write a simply GitHub client with usage of GitHub API (https://developer.github.com/v3/). The application has two screens:
				
1. Main screen - user repositories list and repositories search view based on username
2. Screen with details of selected repository

## Application template

Project template contains:

* MVP architectured
* Configured HTTP client with Retrofit
* Dependency injection with Dagger 2 library

## Application requirements 

Designs for requirement #1

![Starting screen](art/screen_1.png)
![Repositories](art/screen_2.png)
![User has no repos](art/screen_3.png)

1. Implementation of searching repositories based on GitHub username. The list must show
    * Repository name
    * Repository description
    * Amount of users that are watching this repository
    * Amount of repository stars
    * Amount of repository forks

    Searching should base on username and search view should be in the toolbar. 
    Searching should be triggered after pressing the loupe icon and keyboard button.
	
    You can find ready to use .xml designs in the `layouts` package in the project.
    Logic with getting data from network you will find in the `GitHubRepositoriesApi`.

Designs for requirement #2

![Repository details](art/screen_4.png)

2. Implementation of repository details screen

    This screen should contains:
    
    * Repository title and description
    * User name, address (if available), email (if available), avatar

    User can go to this screen after pressing any item from main screen list.
    You should use `fetchUserData` from `GitHubRepositoriesApi` to receive more users data. Screen should be implemented in the new screen. Code should follow on current architecture.

## Criteria for assessing the task

* Following current code style
* Creating commits for each functionality
* Writing unit tests
* Following the current architecture
* Usage of dependency injection


